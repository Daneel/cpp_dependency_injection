#pragma once

#include <cassert>

#ifdef CPP_DEPENDENCY_INJECTION_EXCEPTIONS_ENABLE
#include "exception.hpp"
#include <string>  // IWYU pragma: export
#endif

#ifdef CPP_DEPENDENCY_INJECTION_RTTI_ENABLE
#define CPP_DEPENDENCY_INJECTION_RTTI 1
#else
#define CPP_DEPENDENCY_INJECTION_RTTI 0
#endif

#ifdef CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS_ENABLE
#define CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS 1
#else
#define CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS 0
#endif

#ifdef CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION_ENABLE
#define CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION 1
#else
#define CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION 0
#endif

#ifdef CPP_DEPENDENCY_INJECTION_EXCEPTIONS_ENABLE
#define CPP_DEPENDENCY_INJECTION_EXCEPTIONS 1
#else
#define CPP_DEPENDENCY_INJECTION_EXCEPTIONS 0
#endif

#define CPP_DEPENDENCY_INJECTION_NOEXCEPT_ASSERT(condition, errorMessage) \
	assert((static_cast<void>(errorMessage), static_cast<bool>(condition)))	 // NOLINT

#ifdef CPP_DEPENDENCY_INJECTION_EXCEPTIONS_ENABLE
#define CPP_DEPENDENCY_INJECTION_ASSERT(condition, errorMessage) \
	if (!(condition)) \
	throw Dependencies::Exception(errorMessage)
#else
// Use (void) to silent unused warnings.
#define CPP_DEPENDENCY_INJECTION_ASSERT(condition, errorMessage) \
	CPP_DEPENDENCY_INJECTION_NOEXCEPT_ASSERT(condition, errorMessage)
#endif