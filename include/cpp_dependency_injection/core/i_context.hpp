#pragma once

#include "injection_handle.hpp"	 // IWYU pragma: export
#include "interface_pointer.hpp"  // IWYU pragma: export
#include "cpp_dependency_injection/interfaces/interface_finder.hpp"
#include "cpp_dependency_injection/interfaces/interface_cast.hpp"
#include "cpp_dependency_injection/cpp_dependency_injection_config.hpp"
#include "cpp_dependency_injection/fast_rtti/fast_rtti_manager.hpp"

#include <cinttypes>

namespace Dependencies
{
class IContext
{
public:
	template<typename T>
	[[nodiscard]] InjectionHandle inject(T* rawPtr)
	{
		CPP_DEPENDENCY_INJECTION_ASSERT(rawPtr != nullptr, "IContext: invalid pointer!");
		auto interfaces = InterfaceFinder::getCastMap(rawPtr);
		CPP_DEPENDENCY_INJECTION_ASSERT(interfaces.size() > 0, "IContext: receive invalid cast map!");
		InterfaceCast* interfacesBegin = &*interfaces.begin();
		return injectImpl(interfacesBegin, interfacesBegin + interfaces.size());
	}

	template<typename T>
	[[nodiscard]] InterfacePointer<T> getInterface()
	{
		auto interfaceId = FastRttiManager::instance().getId<T>();
		auto result = getInterfaceImpl(interfaceId);
		return std::move(result).template convertTo<T>();
	}

	[[nodiscard]] virtual InjectionHandle injectImpl(InterfaceCast* interfacesBegin, InterfaceCast* interfacesEnd) = 0;
	[[nodiscard]] virtual InterfacePointer<void> getInterfaceImpl(std::uint32_t typeId) = 0;

	virtual void incRef() = 0;
	virtual void decRef() = 0;
};
}  // namespace Dependencies
