#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/fast_rtti/fast_rtti_manager.hpp>
#include "implements.hpp"
#include "interface_cast.hpp"  // IWYU pragma: export

#include <array>  // IWYU pragma: export

namespace Dependencies
{
template<typename T, typename... Interfaces>
struct implemented_types
{
	implemented_types(T*);
};

class InterfaceFinder
{
public:
	template<typename T>
	static auto getCastMap(T* ptr)
	{
		InterfaceCast primaryCast = {ptr, FastRttiManager::instance().getId<T>()};
		return arrayConcatenate(std::array<InterfaceCast, 1>({primaryCast}), getInterfacesCastMap(ptr));
	}

private:
	template<typename T, typename ImplementedTypes = typename T::ImplementedTypes>
	static auto getInterfacesCastMap(T* ptr)
	{
		return getInterfacesCastMapImpl(ptr, static_cast<ImplementedTypes*>(nullptr));
	}

	static auto getInterfacesCastMap(...)
	{
		return std::array<InterfaceCast, 0>{};
	}

	template<typename T, typename... Interfaces>
	static auto getInterfacesCastMapImpl(T* ptr, std::tuple<Interfaces...>*)
	{
		return arrayConcatenate(getCastMap(static_cast<Interfaces*>(ptr))...);
	}

private:
	template<size_t... SIZES>
	static auto arrayConcatenate(const std::array<InterfaceCast, SIZES>&... arrays)
	{
		constexpr size_t RESULT_SIZE = (SIZES + ...);
		std::array<InterfaceCast, RESULT_SIZE> result;

		auto outputIt = result.begin();
		(([&outputIt, &arrays]() {
			 for (const auto& element : arrays)
			 {
				 (*outputIt) = element;
				 ++outputIt;
			 }
		 }()),
		 ...);

		CPP_DEPENDENCY_INJECTION_ASSERT(outputIt == result.end(), "Dependencies: failed to concatenate array");

		return result;
	}
};
}  // namespace Dependencies