#pragma once

#include <catch2/catch.hpp>
#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/interfaces/interface_cast.hpp>
#include <cpp_dependency_injection/exception.hpp>  // IWYU pragma: export
#include <array>  // IWYU pragma: export
#include <iterator>

#if CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
#include "parallel_checker.hpp"
#endif

namespace Dependencies
{
template<typename StorageType>
class GenericStorageTests
{
public:
	static constexpr std::size_t MAX_INTERFACE_INDEX = 16;

	static void simpleTest()
	{
		StorageType storage;

		int tmp = 0;
		InterfaceCast cast{&tmp, 1};
		storage.add(&cast, std::next(&cast));

		CHECK(storage.requestNewRef(1) == &tmp);
		storage.decRef(1);

		storage.remove(1);
	}

	static void absentInterface()
	{
		StorageType storage;

		CHECK(storage.requestNewRef(1) == nullptr);
		storage.decRef(1);
	}

	static void referenceAfterDelete()
	{
		StorageType storage;

		int tmp = 0;
		InterfaceCast cast{&tmp, 1};
		storage.add(&cast, std::next(&cast));

		storage.remove(1);

		CHECK(storage.requestNewRef(1) == nullptr);
		storage.decRef(1);
	}

	static void addAfterReference()
	{
		StorageType storage;

		CHECK(storage.requestNewRef(1) == nullptr);
		storage.decRef(1);

		int tmp = 0;
		InterfaceCast cast{&tmp, 1};
		storage.add(&cast, std::next(&cast));

		CHECK(storage.requestNewRef(1) == &tmp);
		storage.decRef(1);

		storage.remove(1);
	}

	static void interfaceChain()
	{
		StorageType storage;

		int tmp1 = 0;
		int tmp2 = 0;
		int tmp3 = 0;
		auto casts = std::array<InterfaceCast, 3>{
			InterfaceCast{&tmp1, 1},
			InterfaceCast{&tmp2, 2},
			InterfaceCast{&tmp3, 3},
		};

		auto index = storage.add(&*casts.begin(), std::next(&*casts.begin(), casts.size()));

		CHECK(storage.requestNewRef(1) == &tmp1);
		CHECK(storage.requestNewRef(2) == &tmp2);
		CHECK(storage.requestNewRef(3) == &tmp3);

		storage.decRef(1);
		storage.decRef(2);
		storage.decRef(3);

		storage.remove(index);

		CHECK(storage.requestNewRef(2) == nullptr);
		storage.decRef(2);
	}

#if CPP_DEPENDENCY_INJECTION_EXCEPTIONS
	static void doubleAdd()
	{
		StorageType storage;
		int tmp1 = 0;
		InterfaceCast cast1{&tmp1, 1};
		int tmp2 = 0;
		InterfaceCast cast2{&tmp2, 1};

		auto index = storage.add(&cast1, std::next(&cast1));
		CHECK_THROWS_AS(storage.add(&cast2, std::next(&cast2)), Dependencies::Exception);
		storage.remove(index);
	}
	static void doubleAddInterface()
	{
		StorageType storage;

		int tmp1 = 0;
		int tmp2 = 0;
		int tmp3 = 0;
		auto casts = std::array<InterfaceCast, 3>{
			InterfaceCast{&tmp1, 1},
			InterfaceCast{&tmp2, 2},
			InterfaceCast{&tmp3, 3},
		};

		int tmp4 = 0;
		InterfaceCast cast2{&tmp4, 3};

		auto index = storage.add(&*casts.begin(), std::next(&*casts.begin(), casts.size()));
		CHECK_THROWS_AS(storage.add(&cast2, std::next(&cast2)), Dependencies::Exception);
		storage.remove(index);
	}
	static void addWhileRefencing()
	{
		StorageType storage;
		storage.requestNewRef(1);

		int tmp = 0;
		InterfaceCast cast{&tmp, 1};

		CHECK_THROWS_AS(storage.add(&cast, std::next(&cast)), Dependencies::Exception);

		storage.decRef(1);
	}
	static void removeWhileReferencing()
	{
		StorageType storage;
		int tmp = 0;
		InterfaceCast cast{&tmp, 1};

		auto index = storage.add(&cast, std::next(&cast));
		storage.requestNewRef(1);
		CHECK_THROWS_AS(storage.remove(index), Dependencies::Exception);
		storage.decRef(1);
		CHECK_NOTHROW(storage.remove(index));
	}
#endif

#if CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
	static void parallelAddRemove()
	{
		constexpr size_t THREAD_COUNT = 16;
		constexpr auto TEST_DURATION = std::chrono::seconds(1);
		constexpr auto ITERATION_SIZE = 1000;

		StorageType storage;

		ParallelChacker checker(TEST_DURATION, THREAD_COUNT, ITERATION_SIZE);

		bool ok = checker.check([&storage](std::uint32_t threadId) {
			int tmp = 0;
			InterfaceCast cast{&tmp, threadId};
			storage.add(&cast, std::next(&cast));

			if (storage.requestNewRef(threadId) != &tmp)
			{
				return false;
			}
			storage.decRef(threadId);

			storage.remove(threadId);

			return true;
		});

		CHECK(ok);
	}

	static void parallelRequests()
	{
		constexpr size_t THREAD_COUNT = 16;
		constexpr auto TEST_DURATION = std::chrono::seconds(1);
		constexpr auto ITERATION_SIZE = 1000;

		StorageType storage;

		ParallelChacker checker(TEST_DURATION, THREAD_COUNT, ITERATION_SIZE);

		int tmp = 0;
		InterfaceCast cast{&tmp, 1};
		storage.add(&cast, std::next(&cast));

		bool ok = checker.check([&storage, &tmp](std::uint32_t threadId) {
			if (storage.requestNewRef(1) != &tmp)
			{
				return false;
			}
			storage.decRef(1);

			return true;
		});

		CHECK(ok);

		storage.remove(1);
	}

	static void parallelRequestsToNullptr()
	{
		constexpr size_t THREAD_COUNT = 16;
		constexpr auto TEST_DURATION = std::chrono::seconds(1);
		constexpr auto ITERATION_SIZE = 1000;

		StorageType storage;

		ParallelChacker checker(TEST_DURATION, THREAD_COUNT, ITERATION_SIZE);

		bool ok = checker.check([&storage](std::uint32_t threadId) {
			if (storage.requestNewRef(1) != nullptr)
			{
				return false;
			}
			storage.decRef(1);

			return true;
		});

		CHECK(ok);
	}
#endif
};
}  // namespace Dependencies
