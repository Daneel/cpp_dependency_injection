#pragma once

namespace Dependencies
{
class IDestructible
{
public:
	virtual ~IDestructible() = default;
};
}
