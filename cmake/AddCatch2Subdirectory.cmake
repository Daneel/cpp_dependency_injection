set(CATCH2_DIR ${CMAKE_CURRENT_LIST_DIR}/../external/Catch2)

function(add_catch2_subdirectory)
    add_subdirectory(${CATCH2_DIR})
endfunction()
