#include <catch2/catch.hpp>
#include "generic_storage_tests.hpp"
#include <cpp_dependency_injection/storages/static_interface_storage.hpp>
#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/interfaces/interface_cast.hpp>
#include <iterator>

int static_storage_tests_token = 0;

namespace Dependencies
{
namespace
{
constexpr auto MAX_INTERFACE_INDEX = GenericStorageTests<void>::MAX_INTERFACE_INDEX;

TEST_CASE("StaticInterfaceStorage simple test", "[positive]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::simpleTest();
}

TEST_CASE("StaticInterfaceStorage absent interface", "[positive]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::absentInterface();
}

TEST_CASE("StaticInterfaceStorage reference after delete", "[positive]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::referenceAfterDelete();
}

TEST_CASE("StaticInterfaceStorage add after reference", "[positive]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::addAfterReference();
}

TEST_CASE("StaticInterfaceStorage interface chain", "[positive]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::interfaceChain();
}

#if CPP_DEPENDENCY_INJECTION_EXCEPTIONS
TEST_CASE("StaticInterfaceStorage double add", "[negative]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::doubleAdd();
}

TEST_CASE("StaticInterfaceStorage double add interface", "[negative]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::doubleAddInterface();
}
TEST_CASE("StaticInterfaceStorage add while referencing", "[negative]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::addWhileRefencing();
}
TEST_CASE("StaticInterfaceStorage remove while referencing", "[negative]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::removeWhileReferencing();
}
#endif

#if CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
TEST_CASE("StaticInterfaceStorage parallel add/remove", "[positive]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::parallelAddRemove();
}

TEST_CASE("StaticInterfaceStorage parallel requests", "[positive]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::parallelRequests();
}

TEST_CASE("StaticInterfaceStorage parallel requests to nullptr", "[positive]")
{
	GenericStorageTests<StaticInterfaceStorage<MAX_INTERFACE_INDEX>>::parallelRequestsToNullptr();
}
#endif

TEST_CASE("StaticInterfaceStorage offset simple", "[positive]")
{
	auto storage = StaticInterfaceStorage<2>(10);

	int tmp = 0;

	InterfaceCast cast{&tmp, 11};
	auto index = storage.add(&cast, std::next(&cast));
	auto ptr = storage.requestNewRef(11);
	CHECK(ptr == &tmp);
	storage.decRef(11);
	storage.remove(index);
}

TEST_CASE("StaticInterfaceStorage offset interface chain", "[positive]")
{
	auto storage = StaticInterfaceStorage<3>(10);

	int tmp1 = 0;
	int tmp2 = 0;
	int tmp3 = 0;
	auto casts = std::array<InterfaceCast, 3>{
		InterfaceCast{&tmp1, 10},
		InterfaceCast{&tmp2, 11},
		InterfaceCast{&tmp3, 12},
	};

	auto index = storage.add(&*casts.begin(), std::next(&*casts.begin(), casts.size()));

	CHECK(storage.requestNewRef(10) == &tmp1);
	CHECK(storage.requestNewRef(11) == &tmp2);
	CHECK(storage.requestNewRef(12) == &tmp3);
	storage.decRef(10);
	storage.decRef(11);
	storage.decRef(12);

	storage.remove(index);
}

TEST_CASE("StaticInterfaceStorage offset nullptr request", "[positive]")
{
	auto storage = StaticInterfaceStorage<2>(10);
	storage.requestNewRef(10);
	storage.decRef(10);
}

#if CPP_DEPENDENCY_INJECTION_EXCEPTIONS
TEST_CASE("StaticInterfaceStorage too low index", "[negative]")
{
	auto storage = StaticInterfaceStorage<2>(10);
	CHECK_THROWS(storage.requestNewRef(9));
}

TEST_CASE("StaticInterfaceStorage too high index", "[negative]")
{
	auto storage = StaticInterfaceStorage<2>(10);
	CHECK_THROWS(storage.requestNewRef(12));
}
#endif
}  // namespace
}  // namespace Dependencies
