#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>

#if CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS
#include "i_destructible.hpp"

#include <type_traits>
#include <vector>
#include <memory>

namespace Dependencies
{
class Finalizer
{
	template<typename T>
	class ItemWrapper : public IDestructible
	{
	public:
		ItemWrapper(T&& item)
			: m_item(std::move(item))
		{
		}
		~ItemWrapper()
		{
			if constexpr (std::is_invocable_v<T>)
			{
				m_item();
			}
		}

	private:
		T m_item;
	};

public:
	~Finalizer()
	{
		finalize();
	}

	void finalize()
	{
		while (!m_items.empty())
		{
			m_items.pop_back();
		}
	}

	template<typename T>
	Finalizer& operator+=(T&& item)
	{
		m_items.emplace_back(std::make_unique<ItemWrapper<T>>(std::forward<T>(item)));
		return *this;
	}

private:
	std::vector<std::unique_ptr<IDestructible>> m_items;
};
}  // namespace Dependencies

#endif