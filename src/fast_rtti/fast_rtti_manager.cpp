#include <cpp_dependency_injection/fast_rtti/fast_rtti_manager.hpp>

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/multithreading/lock_guard.hpp>  // IWYU pragma: keep
#include <cpp_dependency_injection/multithreading/mutex.hpp>  // IWYU pragma: keep
#include <utility>

namespace Dependencies
{
FastRttiManager& FastRttiManager::instance()
{
	static FastRttiManager instance;
	return instance;
}
#if CPP_DEPENDENCY_INJECTION_RTTI
std::uint32_t FastRttiManager::getIdByNativeRtti(std::type_index type)
{
	auto lock = LockGuard<Mutex>(m_mutex);

	auto it = m_typeIdentificators.find(type);
	if (it != m_typeIdentificators.end())
	{
		return it->second;
	}

	auto id = generateId();
	m_typeIdentificators[type] = id;
	return id;
}
#endif
}  // namespace Dependencies
