#pragma once

#include "i_context.hpp"

namespace Dependencies
{
class SmartContextPointer
{
public:
	SmartContextPointer() noexcept = default;

	SmartContextPointer(const std::nullptr_t&) noexcept {}

	SmartContextPointer(IContext* context) noexcept
		: m_context(context)
	{
		if (m_context)
		{
			m_context->incRef();
		}
	}

	~SmartContextPointer()
	{
		if (m_context)
		{
			m_context->decRef();
		}
	}

	SmartContextPointer(const SmartContextPointer& other) noexcept
		: m_context(other.m_context)
	{
		if (m_context)
		{
			m_context->incRef();
		}
	}

	SmartContextPointer& operator=(const SmartContextPointer& other) noexcept
	{
		if (m_context)
		{
			m_context->decRef();
		}
		m_context = other.m_context;
		if (m_context)
		{
			m_context->incRef();
		}

		return *this;
	}

	SmartContextPointer(SmartContextPointer&& other) noexcept
		: m_context(other.m_context)
	{
		other.m_context = nullptr;
	}

	SmartContextPointer& operator=(SmartContextPointer&& other) noexcept
	{
		if (m_context)
		{
			m_context->decRef();
		}
		m_context = other.m_context;
		other.m_context = nullptr;

		return *this;
	}

	void reset()
	{
		if (m_context)
		{
			m_context->decRef();
		}
		m_context = nullptr;
	}

	explicit operator bool() const noexcept
	{
		return m_context != nullptr;
	}

	IContext* get() const noexcept
	{
		return m_context;
	}

	IContext* operator->() const noexcept
	{
		return m_context;
	}

	IContext& operator*() const noexcept
	{
		return *m_context;
	}

private:
	IContext* m_context = nullptr;
};
}  // namespace Dependencies
