#pragma once

#include "i_interface_storage.hpp"
#include "cpp_dependency_injection/cpp_dependency_injection_config.hpp"
#include <limits>

namespace Dependencies
{
class InjectionHandle
{
public:
	InjectionHandle() noexcept = default;

	InjectionHandle(IInterfaceStorage& storage, std::uint32_t& typeIndex) noexcept
		: m_storage(&storage)
		, m_typeIndex(typeIndex)
	{
	}

	~InjectionHandle()
	{
		if (m_storage)
		{
			m_storage->remove(m_typeIndex);
		}
	}

	InjectionHandle(const InjectionHandle& other) = delete;
	InjectionHandle& operator=(const InjectionHandle& other) = delete;

	InjectionHandle(InjectionHandle&& other) noexcept
		: m_storage(other.m_storage)
		, m_typeIndex(other.m_typeIndex)
	{
		other.m_storage = nullptr;
		other.m_typeIndex = std::numeric_limits<std::uint32_t>::max();
	}

	InjectionHandle& operator=(InjectionHandle&& other) noexcept
	{
		m_storage = other.m_storage;
		m_typeIndex = other.m_typeIndex;
		other.m_storage = nullptr;
		other.m_typeIndex = std::numeric_limits<std::uint32_t>::max();
		return *this;
	}

	void reset()
	{
		if (m_storage)
		{
			m_storage->remove(m_typeIndex);
		}

		m_storage = nullptr;
		m_typeIndex = std::numeric_limits<std::uint32_t>::max();
	}

	explicit operator bool() const noexcept
	{
		return static_cast<bool>(m_storage);
	}

private:
	IInterfaceStorage* m_storage = nullptr;
	std::uint32_t m_typeIndex = std::numeric_limits<std::uint32_t>::max();
};
}  // namespace Dependencies
