#pragma once

#include "fast_rtti_index.hpp"	// IWYU pragma: export

namespace Dependencies
{
template<typename T>
class FastRtti
{
public:
	static FastRttiIndex rttiIndex;
};
}  // namespace Dependencies
