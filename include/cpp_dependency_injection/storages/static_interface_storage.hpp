#pragma once

#include <cpp_dependency_injection/core/i_interface_storage.hpp>
#include <cpp_dependency_injection/interfaces/interface_cast.hpp>
#include <cpp_dependency_injection/multithreading/atomic.hpp>
#include <limits>
#include <array>

namespace Dependencies
{
template<std::size_t SIZE>
class StaticInterfaceStorage : public IInterfaceStorage
{
	static constexpr auto INVALID_TYPE_INDEX = std::numeric_limits<std::uint32_t>::max();

	struct Item
	{
		void* ptr = nullptr;
		AtomicUint32 refCount = 0;
		std::uint32_t nextTypeIndex = INVALID_TYPE_INDEX;
	};

public:
	explicit StaticInterfaceStorage(std::size_t startIndex = 0)
		: m_startIndex(startIndex)
	{
	}
	~StaticInterfaceStorage()
	{
		for (const auto& interface : m_interfaces)
		{
			CPP_DEPENDENCY_INJECTION_NOEXCEPT_ASSERT(
				interface.refCount == 0, "Cant delete storage, some interface is still has refs!");
		}
	}

	std::uint32_t add(InterfaceCast* interfacesBegin, InterfaceCast* interfacesEnd)
	{
		for (auto* it = interfacesBegin; it != interfacesEnd; ++it)
		{
			auto index = toLocalIndex(it->typeIndex);
			CPP_DEPENDENCY_INJECTION_ASSERT(
				m_interfaces[index].refCount == 0, "Interface is registered or referencing!");
		}

		std::uint32_t prev = INVALID_TYPE_INDEX;
		for (auto* it = interfacesBegin; it != interfacesEnd; ++it)
		{
			auto index = toLocalIndex(it->typeIndex);

			auto& item = m_interfaces[index];
			item.ptr = it->ptr;
			item.refCount = 1;

			if (prev != INVALID_TYPE_INDEX)
			{
				m_interfaces[prev].nextTypeIndex = index;
			}
			prev = index;
		}

		return interfacesBegin->typeIndex;
	}

	void remove(std::uint32_t type) override
	{
		auto index = toLocalIndex(type);

		while (index != INVALID_TYPE_INDEX)
		{
			auto& interface = m_interfaces[index];
			CPP_DEPENDENCY_INJECTION_ASSERT(interface.ptr, "Invalid interface index!");
			CPP_DEPENDENCY_INJECTION_ASSERT(interface.refCount == 1, "Interface still has references!");
			index = interface.nextTypeIndex;
			interface.nextTypeIndex = INVALID_TYPE_INDEX;
			interface.ptr = nullptr;
			interface.refCount = 0;
		}
	}

	void* requestNewRef(std::uint32_t type) override
	{
		auto index = toLocalIndex(type);
		auto& interface = m_interfaces[index];
		++interface.refCount;
		return interface.ptr;
	}

	void incRef(std::uint32_t type) override
	{
		auto index = toLocalIndex(type);
		++m_interfaces[index].refCount;
	}

	void decRef(std::uint32_t type) override
	{
		auto index = toLocalIndex(type);
		auto& interface = m_interfaces[index];
		CPP_DEPENDENCY_INJECTION_ASSERT(interface.refCount > 0, "StaticInterfaceStorage: Invalid dec ref!");
		--interface.refCount;
	}

private:
	std::uint32_t toLocalIndex(std::uint32_t type)
	{
		CPP_DEPENDENCY_INJECTION_ASSERT(type >= m_startIndex, "StaticInterfaceStorage: Type index is out of range!");
		type -= m_startIndex;
		CPP_DEPENDENCY_INJECTION_ASSERT(type < SIZE, "StaticInterfaceStorage: Type index is out of range!");
		return type;
	}

private:
	const std::size_t m_startIndex = 0;
	std::array<Item, SIZE> m_interfaces{};
};
}  // namespace Dependencies
