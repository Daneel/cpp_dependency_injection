#include "catch2/catch.hpp"

#include <cpp_dependency_injection/core/context.hpp>
#include <cpp_dependency_injection/storages/static_interface_storage.hpp>
#include <cpp_dependency_injection/dependencies/soft_dependencies.hpp>
#include <cpp_dependency_injection/dependencies/hard_dependencies.hpp>
#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/fast_rtti/fast_rtti.hpp>

int dependencies_tests_token = 0;

namespace Dependencies
{
#if CPP_DEPENDENCY_INJECTION_EXCEPTIONS
class Exception;
#endif

namespace
{
using ContextType = Context<StaticInterfaceStorage<100>>;

class SimpleClass : public FastRtti<SimpleClass>
{
};

class SecondSimpleClass : public FastRtti<SecondSimpleClass>
{
};
}  // namespace

template<>
FastRttiIndex FastRtti<SecondSimpleClass>::rttiIndex{};

template<>
FastRttiIndex FastRtti<SimpleClass>::rttiIndex{};

namespace
{
TEST_CASE("SoftDependencies simple test", "[positive]")
{
	ContextType ctx;
	SimpleClass instance;
	auto injectHandle = ctx.inject(&instance);
	SoftDependencies<SimpleClass> dependencies(ctx);
	CHECK(dependencies.getInterface<SimpleClass>() == &instance);
}

TEST_CASE("HardDependencies simple test", "[positive]")
{
	ContextType ctx;
	SimpleClass instance;

	auto injectHandle = ctx.inject(&instance);

	HardDependencies<SimpleClass> dependencies(ctx);

	CHECK(&dependencies.refInterface<SimpleClass>() == &instance);
}

TEST_CASE("SoftDependencies multi dependencies", "[positive]")
{
	ContextType ctx;
	SimpleClass instance1;
	SecondSimpleClass instance2;

	auto injectHandle1 = ctx.inject(&instance1);
	auto injectHandle2 = ctx.inject(&instance2);

	SoftDependencies<SimpleClass, SecondSimpleClass> dependencies(ctx);

	CHECK(dependencies.getInterface<SimpleClass>() == &instance1);
	CHECK(dependencies.getInterface<SecondSimpleClass>() == &instance2);
}

TEST_CASE("HardDependencies multi dependencies", "[positive]")
{
	ContextType ctx;
	SimpleClass instance1;
	SecondSimpleClass instance2;

	auto injectHandle1 = ctx.inject(&instance1);
	auto injectHandle2 = ctx.inject(&instance2);

	HardDependencies<SimpleClass, SecondSimpleClass> dependencies(ctx);

	CHECK(&dependencies.refInterface<SimpleClass>() == &instance1);
	CHECK(&dependencies.refInterface<SecondSimpleClass>() == &instance2);
}

TEST_CASE("SoftDependencies nullptr request", "[positive]")
{
	ContextType ctx;

	SoftDependencies<SimpleClass> dependencies(ctx);

	CHECK(dependencies.getInterface<SimpleClass>() == nullptr);
}

#if CPP_DEPENDENCY_INJECTION_EXCEPTIONS
TEST_CASE("HardDependencies missing interface", "[negative]")
{
	ContextType ctx;

	CHECK_THROWS_AS(HardDependencies<SimpleClass>(ctx), Dependencies::Exception);
}
#endif
}  // namespace
}  // namespace Dependencies
