#pragma once

#include <cinttypes>

namespace Dependencies
{
class IInterfaceStorage
{
public:
	virtual void remove(std::uint32_t type) = 0;

	virtual void* requestNewRef(std::uint32_t type) = 0;

	virtual void incRef(std::uint32_t type) = 0;
	virtual void decRef(std::uint32_t type) = 0;
};
}  // namespace Dependencies
