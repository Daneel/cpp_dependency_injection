#pragma once

#include <cpp_dependency_injection/core/i_context.hpp>

namespace Dependencies
{
template<typename... InjectorTypes>
class CombinedInjector : InjectorTypes...  // we can not use tuple here
{
public:
	explicit CombinedInjector(IContext& ctx)
		: InjectorTypes(ctx)...
	{
	}
};
}  // namespace Dependencies
