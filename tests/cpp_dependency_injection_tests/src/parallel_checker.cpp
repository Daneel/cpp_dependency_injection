#include "parallel_checker.hpp"

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>

#if CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION

#include <atomic>
#include <thread>
#include <array>
#include <optional>
#include <cstdint>
#include <chrono>
#include <type_traits>
#include <cstddef>

namespace Dependencies
{
bool ParallelChacker::check(std::function<bool(std::uint32_t threadIndex)> fun)
{
	constexpr size_t MAX_THREAD_COUNT = 64;

	CPP_DEPENDENCY_INJECTION_ASSERT(m_threadCount <= MAX_THREAD_COUNT, "ParallelChacker: Too much threads!");

	std::array<std::optional<std::thread>, MAX_THREAD_COUNT> threads;

	std::atomic_bool ok = true;
	auto startTime = std::chrono::high_resolution_clock::now();
	for (uint32_t threadId = 0; threadId < m_threadCount; ++threadId)
	{
		threads.at(threadId).emplace([this, &ok, startTime, &fun, threadId]() {
			while (std::chrono::high_resolution_clock::now() < startTime + m_duration)
			{
				for (size_t i = 0; i < m_iterationSize; ++i)
				{
					if (!fun(threadId))
					{
						ok = false;
					}
				}
			}
		});
	}

	for (auto& thread : threads)
	{
		if (thread.has_value())
		{
			thread->join();
		}
	}

	return ok;
}

}  // namespace Dependencies
#endif