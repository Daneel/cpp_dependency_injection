#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>

#ifdef CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
#include <mutex>  // IWYU pragma: export
#endif

namespace Dependencies
{
#ifdef CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
using Mutex = std::mutex;
#else
using Mutex = nullptr_t;
#endif
}  // namespace Dependencies