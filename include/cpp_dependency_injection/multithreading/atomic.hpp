#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cinttypes>

#ifdef CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
#include <atomic>  // IWYU pragma: export
#endif

namespace Dependencies
{
#ifdef CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
using AtomicUint32 = std::atomic_uint32_t;
#else
using AtomicUint32 = std::uint32_t;
#endif
}  // namespace Dependencies