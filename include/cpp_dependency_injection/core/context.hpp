#pragma once

#include "i_context.hpp"  // IWYU pragma: export
#include "smart_context_pointer.hpp"  // IWYU pragma: export
#include "cpp_dependency_injection/multithreading/atomic.hpp"
#include "cpp_dependency_injection/cpp_dependency_injection_config.hpp"
#include <cstddef>
#include <utility>	// IWYU pragma: export
#include <array>  // IWYU pragma: export

namespace Dependencies
{
template<typename InterfaceStorageType>
class Context : public IContext
{
public:
	template<typename... Args>
	explicit Context(SmartContextPointer parent = nullptr, Args... args)
		: m_parent(std::move(parent))
		, m_interfaceStorage(std::forward(args)...)
	{
	}

	~Context()
	{
		CPP_DEPENDENCY_INJECTION_NOEXCEPT_ASSERT(!m_refCounter, "Context still has references!");
	}

	InjectionHandle injectImpl(InterfaceCast* interfacesBegin, InterfaceCast* interfacesEnd) override
	{
		auto typeIndex = m_interfaceStorage.add(interfacesBegin, interfacesEnd);
		return InjectionHandle(m_interfaceStorage, typeIndex);
	}

	InterfacePointer<void> getInterfaceImpl(std::uint32_t typeId) override
	{
		auto result = InterfacePointer<void>(m_interfaceStorage, typeId);

		if (result)
		{
			return result;
		}

		if (m_parent)
		{
			auto parentResult = m_parent->getInterfaceImpl(typeId);
			if (parentResult)
			{
				return parentResult;
			}
		}

		return result;
	}

	void incRef() override
	{
		++m_refCounter;
	}

	void decRef() override
	{
		CPP_DEPENDENCY_INJECTION_ASSERT(m_refCounter > 0, "Context : invalid decRef!");
		--m_refCounter;
	}

private:
	SmartContextPointer m_parent;
	InterfaceStorageType m_interfaceStorage;
	AtomicUint32 m_refCounter = 0;
};
}  // namespace Dependencies
