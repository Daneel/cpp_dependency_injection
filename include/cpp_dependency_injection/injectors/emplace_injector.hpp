#pragma once

#include <cpp_dependency_injection/core/i_context.hpp>
#include <cpp_dependency_injection/core/injection_handle.hpp>

#include <utility>
#include <type_traits>

namespace Dependencies
{
template<typename T>
class EmplaceInjector
{
public:
	template<typename... Args>
	EmplaceInjector(IContext& ctx, Args... args)
		: m_value(construct(ctx, std::forward<Args>(args)...))
		, m_injectionHandle(ctx.inject(&m_value))
	{
	}

	EmplaceInjector(const EmplaceInjector&) = delete;
	EmplaceInjector& operator=(const EmplaceInjector&) = delete;
	EmplaceInjector(EmplaceInjector&&) = delete;
	EmplaceInjector& operator=(EmplaceInjector&&) = delete;

	T& ref()
	{
		return m_value;
	}

	const T& ref() const
	{
		return m_value;
	}

private:
	template<typename... Args>
	static T construct(IContext& ctx, Args... args)
	{
		constexpr auto DIRECT_CONSTRACTIBLE = std::is_constructible_v<T, Args...>;
		constexpr auto EXTRA_CONSTRACTIBLE = std::is_constructible_v<T, IContext&, Args...>;
		static_assert(DIRECT_CONSTRACTIBLE || EXTRA_CONSTRACTIBLE, "Can't construct value with this arguments");

		if constexpr (DIRECT_CONSTRACTIBLE)
		{
			return T(std::forward<Args>(args)...);
		}
		else
		{
			return T(ctx, std::forward<Args>(args)...);
		}
	}

private:
	T m_value;
	InjectionHandle m_injectionHandle;
};
}  // namespace Dependencies
