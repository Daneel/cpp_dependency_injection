#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_dll.hpp>
#include <cstdint>

namespace Dependencies
{
class CPP_DEPENDENCY_INJECTION_DLL FastRttiIndex
{
public:
	FastRttiIndex() noexcept;
	~FastRttiIndex() = default;

	FastRttiIndex(const FastRttiIndex&) = delete;
	FastRttiIndex(FastRttiIndex&&) = delete;
	FastRttiIndex& operator=(const FastRttiIndex&) = delete;
	FastRttiIndex& operator=(FastRttiIndex&&) = delete;

	bool operator==(const FastRttiIndex& other) const noexcept
	{
		return m_value == other.m_value;
	}

	bool operator!=(const FastRttiIndex& other) const noexcept
	{
		return m_value != other.m_value;
	}

	std::uint32_t value() const
	{
		return m_value;
	}

private:
	std::uint32_t m_value = 0;
};
}  // namespace Dependencies
