BEGIN {cnt = 0;}
$0~/.*:[0-9]+:[0-9]+: warning: .*\[.*\]/{++cnt;}
$0~/.*:[0-9]+:[0-9]+: error: .*\[.*\]/{++cnt;}
END {print cnt}
