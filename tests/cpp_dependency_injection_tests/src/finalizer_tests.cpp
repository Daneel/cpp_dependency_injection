#include <catch2/catch.hpp>

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/utils/finalizer.hpp>
#include <vector>
#include <memory>

#if CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS
namespace Dependencies
{
TEST_CASE("Finalizer callable", "[positive]")
{
	int callCount = 0;
	Finalizer finalizer;
	finalizer += [&callCount]() { ++callCount; };
	CHECK(callCount == 0);
	finalizer.finalize();
	CHECK(callCount == 1);
}

TEST_CASE("Finalizer auto finalize", "[positive]")
{
	int callCount = 0;
	{
		Finalizer finalizer;
		finalizer += [&callCount]() { ++callCount; };
	}
	CHECK(callCount == 1);
}

TEST_CASE("Finalizer call order", "[positive]")
{
	std::vector<int> order;

	Finalizer finalizer;
	finalizer += [&order]() { order.push_back(1); };
	finalizer += [&order]() { order.push_back(2); };
	finalizer += [&order]() { order.push_back(3); };

	CHECK(order.empty());
	finalizer.finalize();
	CHECK(order.size() == 3);
	CHECK(order[0] == 3);
	CHECK(order[1] == 2);
	CHECK(order[2] == 1);
}

TEST_CASE("Finalizer value item", "[positive]")
{
	class TestItem
	{
	public:
		explicit TestItem(int& callCount)
			: m_callCount(callCount)
		{
		}
		TestItem(const TestItem& other) = delete;
		TestItem(TestItem&& other) = delete;
		TestItem& operator=(const TestItem& other) = delete;
		TestItem& operator=(TestItem&& other) = delete;

		~TestItem()
		{
			++m_callCount;
		}
	private:
		int& m_callCount;
	};

	int callCount = 0;
	{
		Finalizer finalizer;
		finalizer += std::make_unique<TestItem>(callCount);
		CHECK(callCount == 0);
		finalizer.finalize();
		CHECK(callCount == 1);
	}
	CHECK(callCount == 1);
}

}  // namespace Dependencies
#endif