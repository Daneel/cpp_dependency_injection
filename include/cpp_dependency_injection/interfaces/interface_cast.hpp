#pragma once

#include <cinttypes>

namespace Dependencies
{
struct InterfaceCast
{
public:
	bool operator==(const InterfaceCast& other) const
	{
		return this->typeIndex == other.typeIndex && this->ptr == other.ptr;
	}
	bool operator!=(const InterfaceCast& other) const
	{
		return this->typeIndex != other.typeIndex || this->ptr != other.ptr;
	}

public:
	void* ptr = nullptr;
	std::uint32_t typeIndex = 0;
};
}  // namespace Dependencies
