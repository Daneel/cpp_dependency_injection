#pragma once

#include "cpp_dependency_injection/core/interface_pointer.hpp"
#include "cpp_dependency_injection/core/i_context.hpp"
#include <tuple>

namespace Dependencies
{
template<typename... Interfaces>
class SoftDependencies
{
public:
	explicit SoftDependencies(IContext& ctx)
		: m_pointers(ctx.getInterface<Interfaces>()...)
	{
	}

	template<typename T>
	T* getInterface()
	{
		return std::get<InterfacePointer<T>>(m_pointers).get();
	}

private:
	std::tuple<InterfacePointer<Interfaces>...> m_pointers;
};
}  // namespace Dependencies
