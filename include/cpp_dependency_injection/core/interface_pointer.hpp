#pragma once

#include "cpp_dependency_injection/cpp_dependency_injection_config.hpp"
#include "i_interface_storage.hpp"
#include <cinttypes>
#include <cstddef>
#include <limits>
#include <type_traits>

namespace Dependencies
{
template<typename T>
class [[nodiscard]] InterfacePointer
{
	template<typename V>
	friend class InterfacePointer;

public:
	InterfacePointer() noexcept = default;

	InterfacePointer(std::nullptr_t) noexcept {}

	InterfacePointer(IInterfaceStorage & interfaceStorage, std::uint32_t interfaceId) noexcept
		: m_ptr(interfaceStorage.requestNewRef(interfaceId))
		, m_interfaceStorage(&interfaceStorage)
		, m_interfaceIndex(interfaceId)
	{
	}

	~InterfacePointer()
	{
		if (m_interfaceStorage)
		{
			m_interfaceStorage->decRef(m_interfaceIndex);
		}
	}

	InterfacePointer(const InterfacePointer& other) noexcept
		: m_ptr(other.m_ptr)
		, m_interfaceStorage(other.m_interfaceStorage)
		, m_interfaceIndex(other.m_interfaceIndex)
	{
		if (m_interfaceStorage)
		{
			m_interfaceStorage->incRef(m_interfaceIndex);
		}
	}

	InterfacePointer& operator=(const InterfacePointer& other) noexcept
	{
		if (m_interfaceStorage)
		{
			m_interfaceStorage->decRef(m_interfaceIndex);
		}

		m_ptr = other.m_ptr;
		m_interfaceStorage = other.m_interfaceStorage;
		m_interfaceIndex = other.m_interfaceIndex;

		if (m_interfaceStorage)
		{
			m_interfaceStorage->incRef(m_interfaceIndex);
		}

		return *this;
	}

	InterfacePointer(InterfacePointer && other) noexcept
		: m_ptr(other.m_ptr)
		, m_interfaceStorage(other.m_interfaceStorage)
		, m_interfaceIndex(other.m_interfaceIndex)
	{
		other.m_ptr = nullptr;
		other.m_interfaceStorage = nullptr;
		other.m_interfaceIndex = std::numeric_limits<std::uint32_t>::max();
	}

	InterfacePointer& operator=(InterfacePointer&& other) noexcept
	{
		if (m_interfaceStorage)
		{
			m_interfaceStorage->decRef(m_interfaceIndex);
		}

		m_ptr = other.m_ptr;
		m_interfaceStorage = other.m_interfaceStorage;
		m_interfaceIndex = other.m_interfaceIndex;

		other.m_ptr = nullptr;
		other.m_interfaceStorage = nullptr;
		other.m_interfaceIndex = std::numeric_limits<std::uint32_t>::max();

		return *this;
	}

	InterfacePointer& operator=(const std::nullptr_t& other) noexcept
	{
		if (m_interfaceStorage)
		{
			m_interfaceStorage->decRef(m_interfaceIndex);
		}

		m_ptr = nullptr;
		m_interfaceStorage = nullptr;
		m_interfaceIndex = std::numeric_limits<std::uint32_t>::max();

		return *this;
	}

	bool operator==(const InterfacePointer& other) const noexcept
	{
		return m_ptr == other.m_ptr;
	}
	bool operator!=(const InterfacePointer& other) const noexcept
	{
		return m_ptr != other.m_ptr;
	}

	bool operator==(const std::nullptr_t& other) const noexcept
	{
		return m_ptr == nullptr;
	}
	bool operator!=(const std::nullptr_t& other) const noexcept
	{
		return m_ptr != nullptr;
	}

	[[nodiscard]] bool isKeepingInterface() const noexcept
	{
		return m_interfaceIndex != std::numeric_limits<std::uint32_t>::max();
	}

	void reset()
	{
		if (m_interfaceStorage)
		{
			m_interfaceStorage->decRef(m_interfaceIndex);
		}

		m_ptr = nullptr;
		m_interfaceStorage = nullptr;
		m_interfaceIndex = std::numeric_limits<std::uint32_t>::max();
	}

	explicit operator bool() const noexcept
	{
		return static_cast<bool>(m_ptr);
	}

	T* get() const noexcept
	{
		return m_ptr;
	}

	T* operator->() const noexcept
	{
		return m_ptr;
	}

	template<typename V = T>
	typename std::enable_if<!std::is_same_v<V, void>, V>::type& operator*() const noexcept
	{
		return *m_ptr;
	}

	template<typename V>
	[[nodiscard]] typename std::enable_if<std::is_same_v<T, void> || std::is_same_v<V, void>, InterfacePointer<V>>::type
	convertTo() noexcept
	{
		auto result = InterfacePointer<V>();
		result.m_ptr = static_cast<V*>(m_ptr);
		result.m_interfaceStorage = m_interfaceStorage;
		result.m_interfaceIndex = m_interfaceIndex;
		m_ptr = nullptr;
		m_interfaceStorage = nullptr;
		m_interfaceIndex = std::numeric_limits<std::uint32_t>::max();
		return result;
	}

private:
	T* m_ptr = nullptr;
	IInterfaceStorage* m_interfaceStorage = nullptr;
	std::uint32_t m_interfaceIndex = std::numeric_limits<std::uint32_t>::max();
};
}  // namespace Dependencies
