#pragma once

#include <cpp_dependency_injection/core/i_context.hpp>
#include <cpp_dependency_injection/dependencies/hard_dependencies.hpp>
#include "pointer_injector.hpp"

#include <type_traits>

namespace Dependencies
{
template<typename FactoryType, typename InjectType>
class InstancePointerInjector : private HardDependencies<FactoryType>
{
public:
	template<typename... Args>
	InstancePointerInjector(IContext& ctx, Args... args)
		: HardDependencies<FactoryType>(ctx)
		, m_subInjector(ctx, this->template refInterface<FactoryType>().operator()(std::forward<Args>(args)...))
	{
	}

	decltype(auto) ref() const
	{
		return m_subInjector.ref();
	}

private:
	PointerInjector<InjectType> m_subInjector;
};
}  // namespace Dependencies
