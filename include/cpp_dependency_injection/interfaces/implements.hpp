#pragma once

#include <tuple>

namespace Dependencies
{
template<typename... Interfaces>
class Implements : public Interfaces...
{
public:
	using ImplementedTypes = std::tuple<Interfaces...>;
};
}  // namespace Dependencies