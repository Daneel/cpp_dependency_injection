#pragma once

#include <cpp_dependency_injection/core/i_context.hpp>
#include <cpp_dependency_injection/core/injection_handle.hpp>
#include <cpp_dependency_injection/dependencies/hard_dependencies.hpp>

#include <type_traits>

namespace Dependencies
{
template<typename FactoryType, typename InjectType>
class InstanceInjector : private HardDependencies<FactoryType>
{
public:
	template<typename... Args>
	InstanceInjector(IContext& ctx, Args... args)
		: HardDependencies<FactoryType>(ctx)
		, m_instance(this->template refInterface<FactoryType>().operator()(std::forward<Args>(args)...))
		, m_injectionHandle(ctx.inject(&m_instance))
	{
	}

	InstanceInjector(const InstanceInjector&) = delete;
	InstanceInjector& operator=(const InstanceInjector&) = delete;
	InstanceInjector(InstanceInjector&&) = delete;
	InstanceInjector& operator=(InstanceInjector&&) = delete;

	InjectType& ref()
	{
		return m_instance;
	}

	const InjectType& ref() const
	{
		return m_instance;
	}

private:
	InjectType m_instance;
	InjectionHandle m_injectionHandle;
};
}  // namespace Dependencies
