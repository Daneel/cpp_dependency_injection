#!/bin/bash

set -e

mkdir -p build/gcc-main-tests
cd build/gcc-main-tests
cmake ../.. -DCPP_DEPENDENCY_INJECTION_TESTS=ON -DCPP_DEPENDENCY_INJECTION_EXCEPTIONS_ENABLE=YES
cmake --build .
cmake --build . --target test
