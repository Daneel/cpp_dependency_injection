#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_dll.hpp>
#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/multithreading/atomic.hpp>
#include "fast_rtti.hpp"

#include <cstdint>

#if CPP_DEPENDENCY_INJECTION_RTTI
#include <typeindex>
#include <unordered_map>
#include <cpp_dependency_injection/multithreading/mutex.hpp>
#else
#include <type_traits>
#endif

namespace Dependencies
{
class CPP_DEPENDENCY_INJECTION_DLL FastRttiManager
{
public:
	static FastRttiManager& instance();

	std::uint32_t generateId()
	{
		return m_nextId++;
	}

	template<typename T>
	std::uint32_t getId()
	{
#if !CPP_DEPENDENCY_INJECTION_RTTI
		static_assert(
			std::is_base_of_v<FastRtti<T>, T>, "Type is not derived from FastRtti<T> and native rtti is disabled!");
#endif

		if constexpr (std::is_base_of_v<FastRtti<T>, T>)
		{
			return FastRtti<T>::rttiIndex.value();
		}
		else
		{
#if CPP_DEPENDENCY_INJECTION_RTTI
			return getIdByNativeRtti(typeid(T));
#endif
		}
	};

private:
	FastRttiManager() = default;

#if CPP_DEPENDENCY_INJECTION_RTTI
	std::uint32_t getIdByNativeRtti(std::type_index type);
#endif
private:
	AtomicUint32 m_nextId = 0;

#if CPP_DEPENDENCY_INJECTION_RTTI
	Mutex m_mutex;
	std::unordered_map<std::type_index, std::uint32_t> m_typeIdentificators;
#endif
};
}  // namespace Dependencies
