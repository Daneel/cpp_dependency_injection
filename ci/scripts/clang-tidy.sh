#!/bin/bash

set -e

DIRECTORY=`dirname $0`
ABS_DIRECTORY=`realpath $DIRECTORY`

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++

mkdir -p build/clang-tidy
cd build/clang-tidy
cmake ../.. -DCPP_DEPENDENCY_INJECTION_TESTS=ON -DCMAKE_CXX_CLANG_TIDY="clang-tidy"
cmake --build . 2>&1 | tee compilation.log
exit `awk -f $ABS_DIRECTORY/clang-tidy-warning-count.awk compilation.log`
