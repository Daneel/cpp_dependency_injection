find src include tests -name '*.cpp' -o -name '*.hpp' -exec clang-format --output-replacements-xml {} \; | grep "<replacement " >/dev/null
if [ $? -ne 1 ]; then
    echo "ERROR: Code is not match clang-format"
    exit 1
fi
echo "OK: Code is match clang-format"
exit 0