#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>

#if CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION

#include <functional>
#include <chrono>
#include <cstdint>
#include <cstddef>

namespace Dependencies
{
class ParallelChacker
{
public:
	template<typename DurationType>
	ParallelChacker(DurationType duration, size_t threadCount, size_t iterationSize)
		: m_duration(std::chrono::duration_cast<DurationType>(duration))
		, m_threadCount(threadCount)
		, m_iterationSize(iterationSize)
	{
	}

	bool check(std::function<bool(std::uint32_t threadIndex)> fun);

private:
	const std::chrono::milliseconds m_duration = std::chrono::milliseconds(0);
	const size_t m_threadCount = 0;
	const size_t m_iterationSize = 0;
};
}  // namespace Dependencies
#endif
