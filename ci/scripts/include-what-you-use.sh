#!/bin/bash

set -e

DIRECTORY=`dirname $0`
ABS_DIRECTORY=`realpath $DIRECTORY`

mkdir -p build/include-what-you-use
cd build/include-what-you-use
cmake ../.. -DCPP_DEPENDENCY_INJECTION_TESTS=ON -DCMAKE_CXX_INCLUDE_WHAT_YOU_USE="/usr/bin/include-what-you-use;-Xiwyu;--verbose=3;"
cmake --build . 2>&1 | tee compilation.log
exit `awk -f $ABS_DIRECTORY/include-what-you-use-warning-count.awk compilation.log`
