#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

extern int fast_rtti_tests_token;
extern int interfaces_tests_token;
extern int dynamic_storage_tests_token;
extern int static_storage_tests_token;
extern int core_tests_token;
extern int dependencies_tests_token;
extern int injector_tests_token;

int tokens =  // NOLINT
	fast_rtti_tests_token
	+ interfaces_tests_token
	+ dynamic_storage_tests_token
	+ static_storage_tests_token
	+ core_tests_token
	+ dependencies_tests_token
	+ injector_tests_token
;