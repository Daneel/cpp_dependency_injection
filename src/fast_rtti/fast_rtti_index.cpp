#include <cpp_dependency_injection/fast_rtti/fast_rtti_index.hpp>
#include <cpp_dependency_injection/fast_rtti/fast_rtti_manager.hpp>

namespace Dependencies
{
FastRttiIndex::FastRttiIndex() noexcept
	: m_value(FastRttiManager::instance().generateId())
{
}
}  // namespace Dependencies
