#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>

#if CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS

#include <cpp_dependency_injection/core/i_interface_storage.hpp>
#include <cpp_dependency_injection/multithreading/mutex.hpp>
#include <limits>
#include <cstdint>
#include <unordered_map>

namespace Dependencies
{
struct InterfaceCast;

class DynamicInterfaceStorage : public IInterfaceStorage
{
	static constexpr auto INVALID_TYPE_INDEX = std::numeric_limits<std::uint32_t>::max();

	struct Item
	{
		void* ptr = nullptr;
		std::uint32_t refCount = 0;
		std::uint32_t nextTypeIndex = INVALID_TYPE_INDEX;
	};

public:
	~DynamicInterfaceStorage();

	auto add(InterfaceCast* interfacesBegin, InterfaceCast* interfacesEnd) -> std::uint32_t;
	void remove(std::uint32_t type) override;

	void* requestNewRef(std::uint32_t type) override;

	void incRef(std::uint32_t type) override;
	void decRef(std::uint32_t type) override;

private:
	void removeImpl(std::uint32_t type);

private:
	Mutex m_mutex;
	std::unordered_map<std::uint32_t, Item> m_interfaces;
};
}  // namespace Dependencies

#endif
