#include "catch2/catch.hpp"

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>

#include <cpp_dependency_injection/fast_rtti/fast_rtti.hpp>
#include <cpp_dependency_injection/core/context.hpp>
#include <cpp_dependency_injection/storages/static_interface_storage.hpp>
#include <cpp_dependency_injection/injectors/emplace_injector.hpp>
#include <cpp_dependency_injection/injectors/pointer_injector.hpp>
#include <cpp_dependency_injection/injectors/instanace_injector.hpp>
#include <cpp_dependency_injection/injectors/instanace_pointer_injector.hpp>
#include <cpp_dependency_injection/injectors/combined_injector.hpp>

#if CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS
#include <memory>
#endif

int injector_tests_token = 0;

namespace Dependencies
{
namespace
{
using ContextType = Context<StaticInterfaceStorage<100>>;

class SimpleClass : public FastRtti<SimpleClass>
{
};

class SecondSimpleClass : public FastRtti<SecondSimpleClass>
{
};

class ClassWithSimpleArguments : public FastRtti<ClassWithSimpleArguments>
{
public:
	ClassWithSimpleArguments(int a, int b, int c)
		: m_a(a)
		, m_b(b)
		, m_c(c)
	{
	}

	int a() { return m_a; }
	int b() { return m_b; }
	int c() { return m_c; }

private:
	int m_a = 0;
	int m_b = 0;
	int m_c = 0;
};

class ClassWithSimpleArgumentsFactory : public FastRtti<ClassWithSimpleArgumentsFactory>
{
public:
	ClassWithSimpleArguments operator()(int a, int b, int c) { return ClassWithSimpleArguments(a, b, c); }
};

class SimpleClassPointerFactory : public FastRtti<SimpleClassPointerFactory>
{
public:
	SimpleClass* operator()() { return &m_instance; }

private:
	SimpleClass m_instance;
};

class ClassWithCtxArgument : public FastRtti<ClassWithCtxArgument>
{
public:
	ClassWithCtxArgument(IContext& ctx, int a)
		: m_ctx(ctx)
		, m_a(a)
	{
	}
	int a() { return m_a; }
	IContext& ctx() { return m_ctx; }

private:
	IContext& m_ctx;
	int m_a = 0;
};
}  // namespace

template<>
FastRttiIndex FastRtti<SimpleClass>::rttiIndex{};
template<>
FastRttiIndex FastRtti<SecondSimpleClass>::rttiIndex{};
template<>
FastRttiIndex FastRtti<ClassWithSimpleArguments>::rttiIndex{};
template<>
FastRttiIndex FastRtti<ClassWithSimpleArgumentsFactory>::rttiIndex{};
template<>
FastRttiIndex FastRtti<SimpleClassPointerFactory>::rttiIndex{};
template<>
FastRttiIndex FastRtti<ClassWithCtxArgument>::rttiIndex{};

namespace
{
TEST_CASE("EmplaceInjector simple", "[positive]")
{
	ContextType ctx;
	auto injector = EmplaceInjector<SimpleClass>(ctx);
	CHECK(ctx.getInterface<SimpleClass>());
}

TEST_CASE("EmplaceInjector argument forward", "[positive]")
{
	ContextType ctx;

	auto injector = EmplaceInjector<ClassWithSimpleArguments>(ctx, 1, 2, 3);
	CHECK(injector.ref().c() == 3);

	auto interfacePtr = ctx.getInterface<ClassWithSimpleArguments>();
	REQUIRE(interfacePtr);
	CHECK(interfacePtr->b() == 2);
}

TEST_CASE("EmplaceInjector ctx argument syntaxis sugar", "[positive]")
{
	ContextType ctx;
	auto injector = EmplaceInjector<ClassWithCtxArgument>(ctx, 1);
	auto interfacePtr = ctx.getInterface<ClassWithCtxArgument>();
	REQUIRE(interfacePtr);
	CHECK(interfacePtr->a() == 1);
	CHECK(&interfacePtr->ctx() == &ctx);
}

TEST_CASE("PointerInjector simple", "[positive]")
{
	ContextType ctx;
	SimpleClass instance;

	auto injector = PointerInjector(ctx, &instance);
	CHECK(&injector.ref() == &instance);

	auto interfacePtr = ctx.getInterface<SimpleClass>();
	REQUIRE(interfacePtr);
	CHECK(interfacePtr.get() == &instance);
}

#if CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS
TEST_CASE("PointerInjector unique_ptr", "[positive]")
{
	ContextType ctx;

	auto instance = std::make_unique<ClassWithSimpleArguments>(4, 5, 6);
	auto injector = PointerInjector(ctx, std::move(instance));
	CHECK(injector.ref().b() == 5);

	auto interfacePtr = ctx.getInterface<ClassWithSimpleArguments>();
	CHECK(interfacePtr);
}
#endif

TEST_CASE("InstanceInjector", "[positive]")
{
	ContextType ctx;

	auto factoryInjector = EmplaceInjector<ClassWithSimpleArgumentsFactory>(ctx);

	auto instanceInjector = InstanceInjector<ClassWithSimpleArgumentsFactory, ClassWithSimpleArguments>(ctx, 7, 8, 9);

	auto interfacePtr = ctx.getInterface<ClassWithSimpleArguments>();
	REQUIRE(interfacePtr);
	CHECK(interfacePtr->b() == 8);
	CHECK(interfacePtr.get() == &instanceInjector.ref());
}

TEST_CASE("InstancePointerInjector", "[positive]")
{
	ContextType ctx;

	auto factoryInjector = EmplaceInjector<SimpleClassPointerFactory>(ctx);
	auto instanceInjector = InstancePointerInjector<SimpleClassPointerFactory, SimpleClass*>(ctx);

	auto interfacePtr = ctx.getInterface<SimpleClass>();
	REQUIRE(interfacePtr);
	CHECK(interfacePtr.get() == &instanceInjector.ref());
}

TEST_CASE("CombinedInjector", "[positive]")
{
	ContextType ctx;

	auto injector = CombinedInjector<EmplaceInjector<SimpleClass>, EmplaceInjector<SecondSimpleClass>>(ctx);

	auto interfacePtr = ctx.getInterface<SimpleClass>();
	CHECK(interfacePtr);
}
}  // namespace
}  // namespace Dependencies
