#include "catch2/catch.hpp"

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>

#include <cpp_dependency_injection/fast_rtti/fast_rtti.hpp>
#include <cpp_dependency_injection/core/context.hpp>
#include <cpp_dependency_injection/storages/static_interface_storage.hpp>
#include <cpp_dependency_injection/interfaces/implements.hpp>

int core_tests_token = 0;

namespace Dependencies
{
#if CPP_DEPENDENCY_INJECTION_EXCEPTIONS
class Exception;
#endif

namespace
{
using ContextType = Context<StaticInterfaceStorage<100>>;

class SimpleClass : public FastRtti<SimpleClass>
{
};

class SecondClass : public FastRtti<SecondClass>
{
};

class SimpleDerivedClass
	: std::array<int, 10>
	, public Implements<SimpleClass>
	, public FastRtti<SimpleDerivedClass>
{
};

class DoubleDerivedClass
	: std::array<int, 11>
	, public Implements<SimpleDerivedClass>
	, public FastRtti<DoubleDerivedClass>
{
};
}  // namespace
template<>
FastRttiIndex FastRtti<SimpleClass>::rttiIndex{};
template<>
FastRttiIndex FastRtti<SecondClass>::rttiIndex{};
template<>
FastRttiIndex FastRtti<SimpleDerivedClass>::rttiIndex{};
template<>
FastRttiIndex FastRtti<DoubleDerivedClass>::rttiIndex{};
namespace
{
TEST_CASE("Context simple test", "[positive]")
{
	ContextType ctx;

	SimpleClass instance;

	auto injectHandle = ctx.inject(&instance);
	auto interfacePointer = ctx.getInterface<SimpleClass>();

	CHECK(interfacePointer.get() == &instance);
}

TEST_CASE("Context two interfaces test", "[positive]")
{
	ContextType ctx;

	SimpleClass instance1;
	SecondClass instance2;

	auto injectHandle1 = ctx.inject(&instance1);
	auto injectHandle2 = ctx.inject(&instance2);

	auto interfacePointer1 = ctx.getInterface<SimpleClass>();
	auto interfacePointer2 = ctx.getInterface<SecondClass>();

	CHECK(interfacePointer1.get() == &instance1);
	CHECK(interfacePointer2.get() == &instance2);
}

TEST_CASE("Context nullptr request", "[positive]")
{
	ContextType ctx;

	auto interfacePointer = ctx.getInterface<SimpleClass>();
	CHECK(interfacePointer.get() == nullptr);
}

TEST_CASE("Context request after delete", "[positive]")
{
	ContextType ctx;

	SimpleClass instance;

	{
		auto injectHandle = ctx.inject(&instance);
		auto interfacePointer = ctx.getInterface<SimpleClass>();
		CHECK(interfacePointer.get() == &instance);
	}

	auto interfacePointer = ctx.getInterface<SimpleClass>();
	CHECK(interfacePointer.get() == nullptr);
}

TEST_CASE("Context add after request", "[positive]")
{
	ContextType ctx;

	SimpleClass instance;

	{
		auto interfacePointer = ctx.getInterface<SimpleClass>();
		CHECK(interfacePointer.get() == nullptr);
	}

	{
		auto injectHandle = ctx.inject(&instance);
		auto interfacePointer = ctx.getInterface<SimpleClass>();
		CHECK(interfacePointer.get() == &instance);
	}
}

TEST_CASE("Context derived class test", "[positive]")
{
	ContextType ctx;

	SimpleDerivedClass instance;

	auto injectHandle = ctx.inject(&instance);

	auto interfacePointer1 = ctx.getInterface<SimpleDerivedClass>();
	auto interfacePointer2 = ctx.getInterface<SimpleClass>();

	CHECK(interfacePointer1.get() == &instance);
	CHECK(interfacePointer2.get() == static_cast<SimpleClass*>(&instance));
}

TEST_CASE("Context double derived class test", "[positive]")
{
	ContextType ctx;

	DoubleDerivedClass instance;

	auto injectHandle = ctx.inject(&instance);

	auto interfacePointer1 = ctx.getInterface<DoubleDerivedClass>();
	auto interfacePointer2 = ctx.getInterface<SimpleDerivedClass>();
	auto interfacePointer3 = ctx.getInterface<SimpleClass>();

	CHECK(interfacePointer1.get() == &instance);
	CHECK(interfacePointer2.get() == static_cast<SimpleDerivedClass*>(&instance));
	CHECK(interfacePointer3.get() == static_cast<SimpleClass*>(&instance));
}

TEST_CASE("Context simple parent", "[positive]")
{
	ContextType parentCtx;
	ContextType ctx(&parentCtx);

	SimpleClass instance;

	CHECK(ctx.getInterface<SimpleClass>().get() == nullptr);
	auto injectHandle = parentCtx.inject(&instance);
	CHECK(ctx.getInterface<SimpleClass>().get() == &instance);
}

TEST_CASE("Context parallel injection", "[positive]")
{
	ContextType parentCtx;
	ContextType ctx1(&parentCtx);
	ContextType ctx2(&parentCtx);

	SimpleClass commonInstance;
	SecondClass instance1;
	SecondClass instance2;

	auto injectHandle1 = parentCtx.inject(&commonInstance);
	auto injectHandle2 = ctx1.inject(&instance1);
	auto injectHandle3 = ctx2.inject(&instance2);

	CHECK(ctx1.getInterface<SimpleClass>().get() == &commonInstance);
	CHECK(ctx2.getInterface<SimpleClass>().get() == &commonInstance);
	CHECK(ctx1.getInterface<SecondClass>().get() == &instance1);
	CHECK(ctx2.getInterface<SecondClass>().get() == &instance2);
}

TEST_CASE("Context parent chain", "[positive]")
{
	ContextType parentCtx1;
	ContextType parentCtx2(&parentCtx1);
	ContextType parentCtx3(&parentCtx2);
	ContextType ctx(&parentCtx3);

	SimpleClass instance;

	CHECK(ctx.getInterface<SimpleClass>().get() == nullptr);
	auto injectHandle = parentCtx3.inject(&instance);
	CHECK(ctx.getInterface<SimpleClass>().get() == &instance);
}

TEST_CASE("Context interface override", "[negative]")
{
	ContextType parentCtx;
	ContextType ctx(&parentCtx);

	SimpleClass instance1;
	SimpleClass instance2;

	auto injectHandle1 = parentCtx.inject(&instance1);
	auto injectHandle2 = ctx.inject(&instance2);
}

#if CPP_DEPENDENCY_INJECTION_EXCEPTIONS
TEST_CASE("Context nullptr add", "[negative]")
{
	ContextType ctx;
	SimpleClass* instance = nullptr;
	CHECK_THROWS_AS(ctx.inject(instance), Dependencies::Exception);
}
TEST_CASE("Context interface add after reference with parents", "[negative]")
{
	ContextType parentCtx;
	ContextType ctx(&parentCtx);

	SimpleClass instance;

	auto pointer = ctx.getInterface<SimpleClass>();
	CHECK_THROWS_AS(ctx.inject(&instance), Dependencies::Exception);
}
#endif

TEST_CASE("InjectionHandle operations", "[positive]")
{
	ContextType ctx;

	SimpleClass instance;

	auto injectHandle = ctx.inject(&instance);
	CHECK(ctx.getInterface<SimpleClass>().get() == &instance);

	auto injectHandle2 = std::move(injectHandle);  // move
	CHECK(ctx.getInterface<SimpleClass>().get() == &instance);

	InjectionHandle injectHandle3;
	injectHandle3 = std::move(injectHandle2);  // move assigment
	CHECK(ctx.getInterface<SimpleClass>().get() == &instance);

	CHECK(injectHandle3);  // bool

	injectHandle3.reset();
	CHECK(ctx.getInterface<SimpleClass>().get() == nullptr);

	CHECK(!injectHandle3);	// bool
}

TEST_CASE("InterfacePointer operations", "[positive]")
{
	ContextType ctx;

	SimpleClass instance;

	auto injectHandle = ctx.inject(&instance);
	auto interfacePointer = ctx.getInterface<SimpleClass>();

	CHECK(interfacePointer.get() == &instance);
	CHECK(&*interfacePointer == &instance);
	CHECK(interfacePointer->rttiIndex == SimpleClass::rttiIndex);
	CHECK(interfacePointer);
	interfacePointer.reset();
	CHECK(!interfacePointer);

	interfacePointer = ctx.getInterface<SimpleClass>();
	auto interfacePointer2 = interfacePointer;
	CHECK(interfacePointer);
	CHECK(interfacePointer2);
	CHECK(interfacePointer.get() == &instance);

	interfacePointer2 = nullptr;
	CHECK(!interfacePointer2);

	interfacePointer.reset();
	CHECK(!interfacePointer);

	interfacePointer = ctx.getInterface<SimpleClass>();
	interfacePointer2 = interfacePointer;

	CHECK(interfacePointer.get() == &instance);
	CHECK(interfacePointer);
	CHECK(interfacePointer2);
}

TEST_CASE("InterfacePointer isKeepingInterface", "[positive]")
{
	ContextType ctx;

	SimpleClass instance;
	auto injectHandle = ctx.inject(&instance);

	auto interfacePointer = ctx.getInterface<SimpleClass>();
	CHECK(interfacePointer);
	CHECK(interfacePointer.isKeepingInterface());

	interfacePointer.reset();
	injectHandle.reset();
	CHECK(!interfacePointer);
	CHECK(!interfacePointer.isKeepingInterface());

	interfacePointer = ctx.getInterface<SimpleClass>();
	CHECK(!interfacePointer);
	CHECK(interfacePointer.isKeepingInterface());

	interfacePointer.reset();
	CHECK(!interfacePointer);
	CHECK(!interfacePointer.isKeepingInterface());
}

TEST_CASE("SmartContextPointer operations", "[positive]")
{
	ContextType ctx;
	SmartContextPointer pointer(&ctx);
	CHECK(pointer);
	CHECK(pointer.get() == &ctx);
	CHECK(&*pointer == &ctx);
	CHECK(pointer->getInterface<SimpleClass>() == nullptr);

	SmartContextPointer pointer2 = pointer;
	SmartContextPointer pointer3;
	pointer3 = pointer2;

	SmartContextPointer pointer4 = std::move(pointer2);
	CHECK(!pointer2);  // NOLINT
	CHECK(pointer4);

	pointer4.reset();
	CHECK(!pointer4);
}
}  // namespace
}  // namespace Dependencies