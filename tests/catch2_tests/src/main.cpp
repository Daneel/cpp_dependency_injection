#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

int return42()
{
	return 42;
}

TEST_CASE("42", "[positive]")
{
	CHECK(return42() == 42);
}
