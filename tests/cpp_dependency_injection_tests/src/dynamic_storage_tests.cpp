#include "generic_storage_tests.hpp"
#include <catch2/catch.hpp>

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/storages/dynamic_interface_storage.hpp>

int dynamic_storage_tests_token = 0;

#if CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS

namespace Dependencies
{
namespace
{
TEST_CASE("DynamicInterfaceStorage simple test", "[positive]")
{
	GenericStorageTests<DynamicInterfaceStorage>::simpleTest();
}

TEST_CASE("DynamicInterfaceStorage absent interface", "[positive]")
{
	GenericStorageTests<DynamicInterfaceStorage>::absentInterface();
}

TEST_CASE("DynamicInterfaceStorage reference after delete", "[positive]")
{
	GenericStorageTests<DynamicInterfaceStorage>::referenceAfterDelete();
}

TEST_CASE("DynamicInterfaceStorage add after reference", "[positive]")
{
	GenericStorageTests<DynamicInterfaceStorage>::addAfterReference();
}

TEST_CASE("DynamicInterfaceStorage interface chain", "[positive]")
{
	GenericStorageTests<DynamicInterfaceStorage>::interfaceChain();
}

#if CPP_DEPENDENCY_INJECTION_EXCEPTIONS
TEST_CASE("DynamicInterfaceStorage double add", "[negative]")
{
	GenericStorageTests<DynamicInterfaceStorage>::doubleAdd();
}

TEST_CASE("DynamicInterfaceStorage double add interface", "[negative]")
{
	GenericStorageTests<DynamicInterfaceStorage>::doubleAddInterface();
}
TEST_CASE("DynamicInterfaceStorage add while referencing", "[negative]")
{
	GenericStorageTests<DynamicInterfaceStorage>::addWhileRefencing();
}
TEST_CASE("DynamicInterfaceStorage remove while referencing", "[negative]")
{
	GenericStorageTests<DynamicInterfaceStorage>::removeWhileReferencing();
}
#endif

#if CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
TEST_CASE("DynamicInterfaceStorage parallel add/remove", "[positive]")
{
	GenericStorageTests<DynamicInterfaceStorage>::parallelAddRemove();
}

TEST_CASE("DynamicInterfaceStorage parallel requests", "[positive]")
{
	GenericStorageTests<DynamicInterfaceStorage>::parallelRequests();
}

TEST_CASE("DynamicInterfaceStorage parallel requests to nullptr", "[positive]")
{
	GenericStorageTests<DynamicInterfaceStorage>::parallelRequestsToNullptr();
}
#endif
}  // namespace
}  // namespace Dependencies
#endif
