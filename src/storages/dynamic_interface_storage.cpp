#include "cpp_dependency_injection/storages/dynamic_interface_storage.hpp"
#include "cpp_dependency_injection/cpp_dependency_injection_config.hpp"
#include "cpp_dependency_injection/interfaces/interface_cast.hpp"
#include "cpp_dependency_injection/multithreading/lock_guard.hpp"  // IWYU pragma: keep
#include "cpp_dependency_injection/multithreading/mutex.hpp"
#include <utility>
#include <iterator>

#if CPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS

namespace Dependencies
{
std::uint32_t DynamicInterfaceStorage::add(InterfaceCast* interfacesBegin, InterfaceCast* interfacesEnd)
{
	Dependencies::LockGuard<Mutex> lock(m_mutex);

	for (auto* it = interfacesBegin; it != interfacesEnd; std::advance(it, 1))
	{
		CPP_DEPENDENCY_INJECTION_ASSERT(
			m_interfaces.find(it->typeIndex) == m_interfaces.end(), "Interface is referencing!");
	}

	std::uint32_t prev = INVALID_TYPE_INDEX;
	for (auto* it = interfacesBegin; it != interfacesEnd; std::advance(it, 1))
	{
		auto& item = m_interfaces[it->typeIndex];
		item.ptr = it->ptr;
		item.refCount = 1;

		if (prev != INVALID_TYPE_INDEX)
		{
			m_interfaces[prev].nextTypeIndex = it->typeIndex;
		}
		prev = it->typeIndex;
	}

	return interfacesBegin->typeIndex;
}

void DynamicInterfaceStorage::remove(std::uint32_t type)
{
	auto lock = LockGuard<Mutex>(m_mutex);

	removeImpl(type);
}

void DynamicInterfaceStorage::removeImpl(std::uint32_t type)
{
	while (type != INVALID_TYPE_INDEX)
	{
		auto it = m_interfaces.find(type);
		CPP_DEPENDENCY_INJECTION_ASSERT(it != m_interfaces.end(), "Invalid interface index!");
		auto& interface = it->second;
		CPP_DEPENDENCY_INJECTION_ASSERT(interface.refCount == 1, "Interface still has references!");
		type = interface.nextTypeIndex;
		m_interfaces.erase(it);
	}
}

void* DynamicInterfaceStorage::requestNewRef(std::uint32_t type)
{
	auto lock = LockGuard<Mutex>(m_mutex);

	auto& interface = m_interfaces[type];
	++interface.refCount;
	return interface.ptr;
}

void DynamicInterfaceStorage::incRef(std::uint32_t type)
{
	auto lock = LockGuard<Mutex>(m_mutex);

	++m_interfaces[type].refCount;
}

void DynamicInterfaceStorage::decRef(std::uint32_t type)
{
	auto lock = LockGuard<Mutex>(m_mutex);

	auto& interface = m_interfaces[type];
	CPP_DEPENDENCY_INJECTION_ASSERT(interface.refCount > 0, "Invalid interface index!");

	if (interface.ptr == nullptr && interface.refCount == 1)
	{
		removeImpl(type);
	}
	else
	{
		--interface.refCount;
	}
}

DynamicInterfaceStorage::~DynamicInterfaceStorage()
{
	CPP_DEPENDENCY_INJECTION_NOEXCEPT_ASSERT(
		m_interfaces.empty(), "Cant delete storage, some interface is still has refs!");
}
}  // namespace Dependencies

#endif
