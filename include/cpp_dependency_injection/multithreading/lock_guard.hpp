#pragma once

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>

#ifdef CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
#include <mutex>  // IWYU pragma: export
#endif

namespace Dependencies
{
#ifdef CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
template<typename T>
using LockGuard = std::lock_guard<T>;
#else
template<typename T>
class LockGuard
{
public
	LockGuard(const T&) {}
};
#endif
}  // namespace Dependencies
