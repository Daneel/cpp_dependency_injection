#pragma once

#include <cpp_dependency_injection/core/i_context.hpp>
#include <cpp_dependency_injection/core/injection_handle.hpp>

#include <type_traits>

namespace Dependencies
{
template<
	typename PointerType,
	typename DereferenceType = decltype(**(static_cast<PointerType*>(nullptr))),
	typename = std::enable_if_t<std::is_reference_v<DereferenceType>>,
	typename InjectType = std::remove_reference_t<DereferenceType>>
class PointerInjector
{
public:
	PointerInjector(IContext& ctx, PointerType pointer)
		: m_pointer(std::move(pointer))
		, m_injectionHandle(ctx.inject(&*m_pointer))
	{
	}

	InjectType& ref() const
	{
		return *m_pointer;
	}

private:
	PointerType m_pointer;
	InjectionHandle m_injectionHandle;
};
}  // namespace Dependencies
