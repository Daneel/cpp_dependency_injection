#pragma once

#include "cpp_dependency_injection/cpp_dependency_injection_config.hpp"
#include "cpp_dependency_injection/core/interface_pointer.hpp"
#include "cpp_dependency_injection/core/i_context.hpp"
#include <tuple>

namespace Dependencies
{
template<typename... Interfaces>
class HardDependencies
{
public:
	explicit HardDependencies(IContext& ctx)
		: m_pointers(ctx.getInterface<Interfaces>()...)
	{
		CPP_DEPENDENCY_INJECTION_ASSERT(

			(static_cast<bool>(std::get<InterfacePointer<Interfaces>>(m_pointers)) && ...), "Interface is missing!");
	}

	template<typename T>
	T& refInterface()
	{
		return *std::get<InterfacePointer<T>>(m_pointers);
	}

private:
	std::tuple<InterfacePointer<Interfaces>...> m_pointers;
};
}  // namespace Dependencies
