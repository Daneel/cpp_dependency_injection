#include <catch2/catch.hpp>
#include <cpp_dependency_injection/interfaces/interface_finder.hpp>
#include <cpp_dependency_injection/interfaces/implements.hpp>
#include <cpp_dependency_injection/fast_rtti/fast_rtti_manager.hpp>
#include <cpp_dependency_injection/fast_rtti/fast_rtti.hpp>

int interfaces_tests_token = 0;

namespace Dependencies
{
namespace
{
template<typename Interface, typename CastMapType, typename TrueType>
void checkInterface(const CastMapType& castMap, TrueType& instance)
{
	CHECK(
		std::find(
			castMap.begin(),
			castMap.end(),
			InterfaceCast{static_cast<Interface*>(&instance), FastRttiManager::instance().getId<Interface>()}) !=
		castMap.end());
}

namespace Test1
{
class TestClass : public FastRtti<TestClass>
{
};
}  // namespace Test1
}  // namespace

template<>
FastRttiIndex FastRtti<Test1::TestClass>::rttiIndex{};

namespace
{
namespace Test1
{
TEST_CASE("InterfaceFinder simple test", "[positive]")
{
	TestClass instance;
	auto castMap = InterfaceFinder::getCastMap(&instance);

	REQUIRE(castMap.size() == 1);
	checkInterface<TestClass>(castMap, instance);
}
}  // namespace Test1

namespace Test2
{
class Interface : public FastRtti<Interface>
{
};

class TestClass
	: public Implements<Interface>
	, public FastRtti<TestClass>
{
};

}  // namespace Test2
}  // namespace

template<>
FastRttiIndex FastRtti<Test2::Interface>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test2::TestClass>::rttiIndex{};

namespace
{
namespace Test2
{
TEST_CASE("InterfaceFinder one interface", "[positive]")
{
	TestClass instance;
	auto castMap = InterfaceFinder::getCastMap(&instance);

	REQUIRE(castMap.size() == 2);
	checkInterface<TestClass>(castMap, instance);
	checkInterface<Interface>(castMap, instance);
}
}  // namespace Test2

namespace Test3
{
class Interface1 : public FastRtti<Interface1>
{
};

class Interface2 : public FastRtti<Interface2>
{
};

class Interface3 : public FastRtti<Interface3>
{
};

class TestClass
	: public Implements<Interface1, Interface2, Interface3>
	, public FastRtti<TestClass>
{
};

}  // namespace Test3
}  // namespace

template<>
FastRttiIndex FastRtti<Test3::Interface1>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test3::Interface2>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test3::Interface3>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test3::TestClass>::rttiIndex{};

namespace
{
namespace Test3
{
TEST_CASE("InterfaceFinder number of interfaces", "[positive]")
{
	TestClass instance;
	auto castMap = InterfaceFinder::getCastMap(&instance);

	REQUIRE(castMap.size() == 4);
	checkInterface<TestClass>(castMap, instance);
	checkInterface<Interface1>(castMap, instance);
	checkInterface<Interface2>(castMap, instance);
	checkInterface<Interface3>(castMap, instance);
}
}  // namespace Test3

namespace Test4
{
class Interface1 : public FastRtti<Interface1>
{
};

class Interface2 : public FastRtti<Interface2>
{
};

class Interface3
	: public Implements<Interface1, Interface2>
	, public FastRtti<Interface3>
{
};

class TestClass
	: public Implements<Interface3>
	, public FastRtti<TestClass>
{
};

}  // namespace Test4
}  // namespace

template<>
FastRttiIndex FastRtti<Test4::Interface1>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test4::Interface2>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test4::Interface3>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test4::TestClass>::rttiIndex{};

namespace
{
namespace Test4
{
TEST_CASE("InterfaceFinder interface derive", "[positive]")
{
	TestClass instance;
	auto castMap = InterfaceFinder::getCastMap(&instance);

	REQUIRE(castMap.size() == 4);
	checkInterface<TestClass>(castMap, instance);
	checkInterface<Interface1>(castMap, instance);
	checkInterface<Interface2>(castMap, instance);
	checkInterface<Interface3>(castMap, instance);
}
}  // namespace Test4

namespace Test5
{
class Interface1 : public FastRtti<Interface1>
{
};

class Interface2 : public FastRtti<Interface2>
{
};

class Interface3 : public FastRtti<Interface3>
{
};

class Interface4 : public FastRtti<Interface4>
{
};

class Interface12
	: public Implements<Interface1, Interface2>
	, public FastRtti<Interface12>
{
};

class Interface34
	: public Implements<Interface3, Interface4>
	, public FastRtti<Interface34>
{
};

class TestClass
	: public Implements<Interface12, Interface34>
	, public FastRtti<TestClass>
{
};
}  // namespace Test5
}  // namespace

template<>
FastRttiIndex FastRtti<Test5::Interface1>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test5::Interface2>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test5::Interface3>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test5::Interface4>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test5::Interface12>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test5::Interface34>::rttiIndex{};
template<>
FastRttiIndex FastRtti<Test5::TestClass>::rttiIndex{};

namespace
{
namespace Test5
{
TEST_CASE("InterfaceFinder interface simple tree", "[positive]")
{
	TestClass instance;
	auto castMap = InterfaceFinder::getCastMap(&instance);

	REQUIRE(castMap.size() == 7);
	checkInterface<TestClass>(castMap, instance);
	checkInterface<Interface12>(castMap, instance);
	checkInterface<Interface34>(castMap, instance);
	checkInterface<Interface1>(castMap, instance);
	checkInterface<Interface2>(castMap, instance);
	checkInterface<Interface3>(castMap, instance);
	checkInterface<Interface4>(castMap, instance);
}
}  // namespace Test5
}  // namespace
}  // namespace Dependencies