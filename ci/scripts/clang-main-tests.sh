#!/bin/bash

set -e

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++

mkdir -p build/clang-main-tests
cd build/clang-main-tests
cmake ../.. -DCPP_DEPENDENCY_INJECTION_TESTS=ON -DCPP_DEPENDENCY_INJECTION_EXCEPTIONS_ENABLE=YES
cmake --build .
cmake --build . --target test

