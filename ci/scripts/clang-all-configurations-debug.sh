#!/bin/bash

set -e

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++

mkcdir ()
{
    mkdir -p -- "$1" &&
    cd -P -- "$1"
}

run_tests()
{
    mkcdir ${1}
    shift
    cmake ../.. -DCMAKE_CXX_FLAGS_DEBUG="-g -Wall -D_DEBUG -D_GLIBCXX_DEBUG" -DCPP_DEPENDENCY_INJECTION_TESTS=ON -DCPP_DEPENDENCY_INJECTION_EXCEPTIONS_ENABLE=YES ${@}
    cmake --build .
    valgrind --error-exitcode=1 --leak-check=full --trace-children=yes ./tests/cpp_dependency_injection_tests/cpp_dependency_injection_tests
    cd ../..
}

run_tests build/gcc-tests-full
run_tests build/gcc-tests-no-rtti -DCPP_DEPENDENCY_INJECTION_RTTI_ENABLE=NO
run_tests build/gcc-tests-no-dynamic -DCPP_DEPENDENCY_INJECTION_RTTI_ENABLE=NO -DCPP_DEPENDENCY_INJECTION_DYNAMIC_ALLOCATIONS_ENABLE=NO
run_tests build/gcc-tests-no-threads -DCPP_DEPENDENCY_INJECTION_THREAD_PROTECTION_ENABLE=NO
run_tests build/gcc-tests-no-exceptions -DCPP_DEPENDENCY_INJECTION_EXCEPTIONS_ENABLE=NO

