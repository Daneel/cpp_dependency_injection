#pragma once

#include <exception>
#include <string>

namespace Dependencies
{
class Exception : public std::exception
{
public:
	Exception(std::string message)
		: m_message(std::move(message))
	{
	}

	const char* what() const noexcept override
	{
		return m_message.c_str();
	}

private:
	std::string m_message;
};
}  // namespace Dependencies