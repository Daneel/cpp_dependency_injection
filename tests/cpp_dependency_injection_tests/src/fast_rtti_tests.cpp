#include <catch2/catch.hpp>

#include <cpp_dependency_injection/cpp_dependency_injection_config.hpp>
#include <cpp_dependency_injection/fast_rtti/fast_rtti.hpp>
#include <cpp_dependency_injection/fast_rtti/fast_rtti_manager.hpp>

int fast_rtti_tests_token = 0;

namespace Dependencies
{
namespace
{
class TestClass1 : public FastRtti<TestClass1>
{
};

class TestClass2 : public FastRtti<TestClass2>
{
};

class TestClass3
	: public TestClass1
	, public FastRtti<TestClass3>
{
};
}  // namespace

template<>
FastRttiIndex FastRtti<TestClass1>::rttiIndex{};

template<>
FastRttiIndex FastRtti<TestClass2>::rttiIndex{};

template<>
FastRttiIndex FastRtti<TestClass3>::rttiIndex{};

namespace
{
TEST_CASE("Fast Rtti simple", "[positive]")
{
	CHECK(FastRttiManager::instance().getId<TestClass1>() == FastRttiManager::instance().getId<TestClass1>());
}

TEST_CASE("Fast Rtti simple negative", "[negative]")
{
	CHECK(FastRttiManager::instance().getId<TestClass1>() != FastRttiManager::instance().getId<TestClass2>());
}

TEST_CASE("Fast Rtti derive", "[positive]")
{
	CHECK(FastRttiManager::instance().getId<TestClass3>() == FastRttiManager::instance().getId<TestClass3>());
	CHECK(FastRttiManager::instance().getId<TestClass3>() != FastRttiManager::instance().getId<TestClass1>());
}

#if CPP_DEPENDENCY_INJECTION_RTTI

TEST_CASE("Fast Rtti native fallback", "[positive]")
{
	class TestClass
	{
	};

	CHECK(FastRttiManager::instance().getId<TestClass>() == FastRttiManager::instance().getId<TestClass>());
}

TEST_CASE("Fast Rtti derive native fallback", "[positive]")
{
	class TestClass1
	{
	};

	class TestClass2 : public TestClass1
	{
	};

	CHECK(FastRttiManager::instance().getId<TestClass2>() == FastRttiManager::instance().getId<TestClass2>());
	CHECK(FastRttiManager::instance().getId<TestClass1>() == FastRttiManager::instance().getId<TestClass1>());
	CHECK(FastRttiManager::instance().getId<TestClass1>() != FastRttiManager::instance().getId<TestClass2>());
}

#if CPP_DEPENDENCY_INJECTION_THREAD_PROTECTION
TEST_CASE("Fast Rtti thread protection", "[negative]")
{
	CHECK(true);
}
#endif

#endif
}  // namespace
}  // namespace Dependencies
